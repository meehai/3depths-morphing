import sys
import numpy as np

from argparse import ArgumentParser
from video_representations_extractor import VideoRepresentationsExtractor
from representations.dummy_representation import DummyRepresentation
from main_generate_data_from_video import ScaleDepth
from media_processing_lib.video import tryReadVideo
from collections import OrderedDict
from pathlib import Path
from matplotlib.cm import hot

sys.path.append("/scratch/nvme0n1/ngc/video-representations-extractor")

def makeImageFn(x):
	y = x["data"]
	assert y.min() >= 0 and y.max() <= 1
	y = hot(y)[..., 0 : 3]
	y = np.uint8(y * 255)
	return y

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("--video", required=True)
	parser.add_argument("--unscaledDir", required=True)
	parser.add_argument("--metricDir", required=True)
	parser.add_argument("--outputDir", required=True)
	args = parser.parse_args()
	args.unscaledDir = Path(args.unscaledDir).absolute()
	args.metricDir = Path(args.metricDir).absolute()
	args.outputDir = Path(args.outputDir).absolute()
	return args

def main():
	args = getArgs()
	tsr = OrderedDict()
	video = tryReadVideo(args.video, vidLib="pims")
	print(" - Unscaled path: %s.\n - Metric path: %s - \nOut path: %s" % \
		(args.unscaledDir, args.metricDir, args.outputDir))

	inDir, inRepr = args.unscaledDir.parents[0], args.unscaledDir.name
	outDir, outRepr = args.outputDir.parents[0], args.outputDir.name
	metricDir, metricRepr = args.metricDir.parents[0], args.metricDir.name
	# assert inDir == outDir
	tsr[metricRepr] = DummyRepresentation(name=metricRepr, makeImageFn=makeImageFn)
	tsr[inRepr] = DummyRepresentation(name=inRepr, makeImageFn=makeImageFn)
	tsr[outRepr] = ScaleDepth(name=outRepr, dependencies=[metricRepr, inRepr], \
		dependencyAliases=["metric", "unscaled"], windowSize=5, invalidPercentage=20)
	# tsr[metricRepr] = DummyRepresentation(metricDir, metricRepr, {}, video, [240, 426], makeImageFn)
	# tsr[inRepr] = DummyRepresentation(inDir, inRepr, {}, video, [240, 426], makeImageFn)
	# tsr[outRepr] = ScaleDepth(outDir, name=outRepr, \
	# 	dependencies={"unscaled" : tsr[inRepr], "metric" : tsr[metricRepr]}, \
	# 	video=video, outShape=[240, 426], windowSize=5, invalidPercentage=20)
	# def __init__(self, video:MPLVideo, outputDir:Path, outputResolution:Tuple[int, int], \
	# 	representations:Dict[str, Union[str, Representation]]):

	# collageOrder = ["test", "sfm", "mask"]
	vre = VideoRepresentationsExtractor(video, args.outputDir, [240, 426], tsr)
	vre.doExport()

if __name__ == "__main__":
	main()


if __name__ == "__main__":
	main()