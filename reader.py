import numpy as np
from typing import Dict, List
from nwdata import Reader, RandomIndexReader, MergeBatchedReader, StaticBatchedReader, DebugReader
from pathlib import Path
from natsort import natsorted
from overrides import overrides
from functools import partial

def mergeFn(x):
	assert "data" in x[0].keys()
	MB = len(x)
	K = x[0]["data"].keys()
	batch = {k : np.array([x[i]["data"][k] for i in range(MB)]) for k in K}
	return {"data":batch, "labels":batch}

def f(dataset, ix, k):
	item = np.load(dataset[k][ix], allow_pickle=True)["arr_0"]
	if item.dtype == np.object:
		item = item.item()
		assert isinstance(item, dict)
		assert "data" in item
		item = item["data"]
	assert item.dtype in (np.uint8, np.float32)
	assert item.min() >= 0 and item.max() <= 1
	return item

class Reader3Depths(Reader):
	def __init__(self, baseDir:str, desiredDims:List[str]=None):
		self.files = self.getFiles(Path(baseDir), desiredDims)
		dataDims = list(self.files.keys())
		dataBuckets = {"data" : dataDims}
		super().__init__(
			dataBuckets=dataBuckets, \
			dimGetter={k : partial(f, k=k) for k in dataDims}, \
			dimTransform={}
		)

	def getFiles(self, baseDir:Path, desiredDims:List[str]):
		Dirs = [x for x in filter(lambda x : x.is_dir(), baseDir.iterdir())]
		dataDims = [str(x).split("/")[-1] for x in Dirs]
		print("[Reader3Depths] Found data dims: %s" % dataDims)
		desiredDims = dataDims if desiredDims is None else desiredDims
		assert "rgb" in desiredDims
		files = {}
		for Dir, D in zip(Dirs, dataDims):
			if not D in desiredDims:
				print("[Reader3Depths::getFiles] Skipping '%s' because not in desired dims: %s" % (D, desiredDims))
				continue
			dirFiles = natsorted([str(x) for x in Dir.glob("*.npz")])
			if len(dirFiles) == 0:
				print("[Reader3Depths::getFiles] Skipping '%s' because 0 npz files were found" % D)
				continue
			files[D] = dirFiles
		print("[Reader3Depths] Final data dims: %s" % list(files.keys()))
		Lens = {k:len(x) for k, x in files.items()}
		assert np.std(list(Lens.values())) == 0, Lens
		return files

	@overrides
	def getDataset(self):
		return self.files

	@overrides
	def __len__(self) -> int:
		K = list(self.files.keys())[0]
		return len(self.files[K])

def getReader(baseDir:Path, trainCfg:Dict, debug:bool, randomize:bool):
	reader = Reader3Depths(baseDir=baseDir, desiredDims=trainCfg["trainAlgorithm"]["dims"])
	if randomize:
		reader = RandomIndexReader(reader, seed=42)
	if debug:
		reader = DebugReader(reader, N=1000)
	reader = MergeBatchedReader(reader, mergeFn=mergeFn)
	reader = StaticBatchedReader(reader, batchSize=trainCfg["batchSize"])
	
	print(reader)
	return reader