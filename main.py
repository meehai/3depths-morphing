import yaml
import numpy as np
from nwdata.utils import fullPath, changeDirectory
from argparse import ArgumentParser
from tqdm import trange

from reader import getReader
from models import getModel

def getArgs(parser=None):
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("--datasetPath", required=True, help="Path to root directory of npz files")
	parser.add_argument("--modelCfg", required=True)
	parser.add_argument("--trainCfg", required=True)
	parser.add_argument("--dir", required=True)
	parser.add_argument("--validationDatasetPath")
	parser.add_argument("--weightsFile")
	parser.add_argument("--debug", type=int, default=0)
	parser.add_argument("--randomizeData", type=int, default=1)
	args = parser.parse_args()

	args.modelCfg = yaml.load(open(fullPath(args.modelCfg), "r"), Loader=yaml.SafeLoader)
	args.trainCfg = yaml.load(open(fullPath(args.trainCfg), "r"), Loader=yaml.SafeLoader)
	args.dir = fullPath(args.dir)
	args.datasetPath = fullPath(args.datasetPath)
	if not args.weightsFile is None:
		args.weightsFile = fullPath(args.weightsFile)
	args.validationDatasetPath = fullPath(args.validationDatasetPath) \
		if not args.validationDatasetPath is None else None
	if args.type in ("test", "export_npz"):
		args.randomizeData = 0
	return args

def main():
	args = getArgs()
	reader = getReader(args.datasetPath, args.trainCfg, args.debug, args.randomizeData)
	model = getModel(args.modelCfg, args.trainCfg)
	valGenerator = getReader(args.validationDatasetPath, args.trainCfg, args.debug, args.randomizeData).iterate() \
		if not args.validationDatasetPath is None else None

	if args.type == "train":
		changeDirectory(args.dir, expectExist=False)
		model.trainGenerator(reader.iterate(), args.trainCfg["numEpochs"], valGenerator)
	elif args.type == "retrain":
		model.loadModel(args.weightsFile)
		changeDirectory(args.dir, expectExist=True)
		model.trainGenerator(reader.iterate(), args.trainCfg["numEpochs"], valGenerator)
	elif args.type == "test":
		model.loadModel(args.weightsFile)
		model.eval()
		changeDirectory(args.dir, expectExist=True)
		res = model.testGenerator(reader.iterate())
		print(res)
	elif args.type == "export_npz":
		model.loadModel(args.weightsFile)
		model.eval()
		changeDirectory(args.dir, expectExist=False)
		ix = 0
		for i in trange(len(reader)):
			data = reader[i]["data"]
			res = model.npForward(data)
			res = np.clip(res, 0, 1)
			for j in range(len(res)):
				savePath = args.dir / ("%d.npz" % ix)
				item = res[j]
				if item.shape[0] == 1:
					item = item[0]
				if item.shape[-1] == 1:
					item = item[..., 0]
				assert len(item.shape) == 2
				np.savez_compressed(savePath, item)
				ix += 1
	else:
		assert False

if __name__ == "__main__":
	main()